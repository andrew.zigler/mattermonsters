import React from "react"

export const Context = React.createContext({})

export const ContextProvider = ({ children }) => {
  const [myContext, updateMyContext] = React.useState({
    page: 1,

    releasable: false,
    monsterName: "",

    mouthIndex: 1,
    eyesIndex: 1,
    accessoryIndex: 1,
    clothingIndex: 1,
    bodyIndex: 1,

    mouthImage: require("../images/Z1_Mouth/Mouth01@2x.png"),
    eyesImage: require("../images/Z2_Eyes/Eyes01@2x.png"),
    accessoryImage: require("../images/Z3_Accessory/Accessory01@2x.png"),
    clothingImage: require("../images/Z4_Clothing/Clothing01@2x.png"),
    bodyImage: require("../images/Z5_Body/Body01@2x.png"),
  })
  const context = {
    myContext,
    updateMyContext,
  }
  return <Context.Provider value={context}>{children}</Context.Provider>
}
