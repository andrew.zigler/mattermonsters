import React from "react"
import { ReleaseForm } from "./ReleaseForm"
import { Chooser } from "./Chooser"
import { Context } from "./Context"

import { monsterManifest } from "../data"

console.log(monsterManifest)

function MonsterMaker() {
  const { myContext } = React.useContext(Context)

  return (
    <main id="monster-maker">
      <div className="left">
        <div className="canvas-wrapper">
          <div id="canvas">
            {monsterManifest.map(({ label }) => (
              <img
                key={label}
                className={label}
                src={myContext[`${label}Image`]}
                alt={`Monster ${label}`}
              />
            ))}
            <img id="polaroid" src={require("../images/polaroid.png")} alt="" />
            <img id="mm-logo" src={require("../images/mm_logo.png")} alt="" />
          </div>
        </div>
        <div id="controls">
          <h3 className="instructions">Customize your Mattermonster:</h3>
          {[...monsterManifest].reverse().map(({ label, images }) => (
            <Chooser
              key={label}
              label={label[0].toUpperCase() + label.substring(1)}
              images={images}
              indexPropName={`${label}Index`}
              imagePropName={`${label}Image`}
            />
          ))}
        </div>
      </div>
      <div className="right">
        <ReleaseForm />
      </div>
    </main>
  )
}

export { MonsterMaker }
