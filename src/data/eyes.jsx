import img1 from "../images/Z2_Eyes/Eyes01@2x.png"
import img2 from "../images/Z2_Eyes/Eyes02@2x.png"

const images = [img1, img2]

export const eyesData = {
  label: "eyes",
  images,
  zIndex: 2,
}
