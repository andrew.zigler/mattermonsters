const req = require.context("./", true, /.jsx$/)
const modules = req.keys().map(req)

export const monsterManifest = modules
  .map((module) => {
    let partData
    for (const prop in module) {
      if (module[prop].label) {
        partData = module[prop]
      }
    }
    return partData
  })
  .sort((partA, partB) => {
    if (partA.zIndex <= partB.zIndex) {
      return 1
    } else {
      return -1
    }
  })
